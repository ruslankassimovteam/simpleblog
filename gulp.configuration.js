var configuration = {
    // Global Gulp configuration
    gulp_tasks_path : 'gulp_tasks',
    bower_path : 'bower_components',

    modules : {},

    addModule : function (name, cb) {
        var module = {};
        this.modules[name] = cb({});
    }
};

configuration.addModule(
    'main',
    function (m) {
        // SASS configuration
        m.sass_root_path = './compiled_assets/scss';
        m.sass_main_file = 'style.scss';
        m.sass_output_path = './web';
        m.sass_output_name = 'main';

        // JS configuration
        m.js_root_path = './compiled_assets/js';
        m.js_include_order = [
            // BOWER LIBS
            configuration.bower_path + '/jquery/dist/jquery.min.js',

            // YOUR CODE
            m.js_root_path + '/**/*.js'
        ];
        m.js_output_path = './web';
        m.js_output_name = 'main';

        return m;
    }
);

module.exports = configuration;