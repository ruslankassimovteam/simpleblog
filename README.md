SimpleBlog
==========

# Installation

```bash
composer install
npm isntall
bower install
gulp

php bin/console d:s:u -f
php bin/console c:c -e prod
```

## Create new administrator

```bash
php bin/console fos:user:create admin admin@admin.com 1234
```