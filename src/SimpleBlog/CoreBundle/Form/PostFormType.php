<?php

namespace SimpleBlog\CoreBundle\Form;

use SimpleBlog\CoreBundle\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class, [
                'label' => 'Titre',
            ]
        );

        $builder->add(
            'content',
            TextareaType::class, [
                'label' => 'Contenu',
            ]
        );

        $builder->add(
            'submit',
            SubmitType::class, [
                'label' => (empty($options['edit']) ? 'Créer' : 'Modifier'),
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => Post::class,
                'edit' => false,
            ]
        );
    }

    public function getParent()
    {
        return FormType::class;
    }
}