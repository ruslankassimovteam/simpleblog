<?php

namespace SimpleBlog\CoreBundle\Controller;

use SimpleBlog\CoreBundle\Entity\Post;
use SimpleBlog\CoreBundle\Form\PostFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $post = new Post();

        $post_form = $this->createForm(PostFormType::class, $post);
        $post_form->handleRequest($request);

        if ($post_form->isSubmitted() && $post_form->isValid()) {
            $em->persist($post);
            $em->flush();

            return $this->redirect($this->generateUrl('admin'));
        }

        return $this->render(
            'CoreBundle:post:form.html.twig',
            [
                'post_form' => $post_form->createView(),
            ]
        );
    }

    public function editAction(Request $request, $postId)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $postRepository = $em->getRepository('CoreBundle:Post');

        $post = $postRepository->findOneById($postId);

        if ($post) {
            $post_form = $this->createForm(PostFormType::class, $post, ['edit' => true]);
            $post_form->handleRequest($request);

            if ($post_form->isSubmitted() && $post_form->isValid()) {
                $em->persist($post);
                $em->flush();

                return $this->redirect($this->generateUrl('admin'));
            }

            return $this->render(
                'CoreBundle:post:form.html.twig',
                [
                    'post_form' => $post_form->createView(),
                ]
            );
        } else {
            return $this->redirect($this->generateUrl('admin'));
        }
    }

    public function removeAction(Request $request, $postId)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $postRepository = $em->getRepository('CoreBundle:Post');

        $post = $postRepository->findOneById($postId);

        if ($post) {
            $em->remove($post);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin'));
    }
}
