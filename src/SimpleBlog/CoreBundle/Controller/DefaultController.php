<?php

namespace SimpleBlog\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $postRepository = $em->getRepository('CoreBundle:Post');

        $posts = $postRepository->findAll();

        return $this->render(
            'CoreBundle::index.html.twig',
            [
                'posts' => $posts
            ]
        );
    }

    public function adminAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $postRepository = $em->getRepository('CoreBundle:Post');

        $posts = $postRepository->findAll();

        return $this->render(
            'CoreBundle::administration.html.twig',
            [
                'posts' => $posts
            ]
        );
    }
}
